package src;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Estagio{

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    private String dataInicio;
    private String dataFim;
    private int duracao;

    @OneToMany(mappedBy="Estagio")
    private List<Avaliacao> avaliacoes = new ArrayList<>();

    @ManyToOne
    private Colaborador colaborador;

    @ManyToOne
    private PropostasEstagio proposta;

    @ManyToOne
    private Tutor tutor;

    @OneToOne
    private Aluno aluno;

    public Estagio(String dataInicio, String dataFim, int duracao, List<Avaliacao> avaliacoes, Colaborador colaborador, PropostasEstagio proposta, Tutor tutor, Aluno aluno) {
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.duracao = duracao;
        this.avaliacoes = avaliacoes;
        this.colaborador = colaborador;
        this.proposta = proposta;
        this.tutor = tutor;
        this.aluno = aluno;
    }

    public Estagio() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public List<Avaliacao> getAvaliacoes() {
        return avaliacoes;
    }

    public void setAvaliacoes(List<Avaliacao> avaliacoes) {
        this.avaliacoes = avaliacoes;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public PropostasEstagio getProposta() {
        return proposta;
    }

    public void setProposta(PropostasEstagio proposta) {
        this.proposta = proposta;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }


}