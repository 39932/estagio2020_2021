package src;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Candidatura{

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int candidaturaID;
    private int preferenciasEstagios;
    private int preferenciaEmpresa;

    @ManyToOne
    private Aluno aluno;

    @ManyToOne
    private Fase fase;

    @ManyToOne
    private PropostasEstagio proposta;

    public int getPreferenciaEmpresa() {
        return preferenciaEmpresa;
    }

    public void setPreferenciaEmpresa(int preferenciaEmpresa) {
        this.preferenciaEmpresa = preferenciaEmpresa;
    }

    public Candidatura(){
        preferenciaEmpresa=0;
    }

    public Candidatura(Aluno aluno, PropostasEstagio proposta ){
        this.aluno = aluno;
        this.proposta = proposta;
        preferenciaEmpresa=0;
    }

    public Candidatura(Aluno aluno, PropostasEstagio proposta, Fase fase) {
        this.aluno = aluno;
        this.proposta = proposta;
        this.fase=fase;
        preferenciaEmpresa=0;
    }


    public int getCandidaturaID() {
        return candidaturaID;
    }

    public void setCandidaturaID(int candidaturaID) {
        this.candidaturaID = candidaturaID;
    }

    public int getPreferenciasEstagios() {
        return preferenciasEstagios;
    }

    public void setPreferenciasEstagios(int preferenciasEstagios) {
        this.preferenciasEstagios = preferenciasEstagios;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Fase getFase() {
        return fase;
    }

    public void setFase(Fase fase) {
        this.fase = fase;
    }

    public PropostasEstagio getProposta() {
        return proposta;
    }

    public void setProposta(PropostasEstagio proposta) {
        this.proposta = proposta;
    }

    public String toString(){
        return aluno.getNome() + " "+ proposta.getNomeEstagio();
    }
}