package src;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/criar-aluno").setViewName("aluno.html");
		registry.addViewController("/registo-aluno").setViewName("registoAluno.html");
		registry.addViewController("/registo-empresa").setViewName("registoEmpresa.html");
		registry.addViewController("/registo-tutor").setViewName("registoTutor.html");
		registry.addViewController("/criar-colaborador").setViewName("colaborador.html");
		registry.addViewController("/criar-empresa").setViewName("empresa.html");
		registry.addViewController("/criar-proposta").setViewName("proposta.html");
		registry.addViewController("/criar-tutor").setViewName("tutor.html");
		registry.addViewController("/criar-fase").setViewName("fase.html");

	}

}