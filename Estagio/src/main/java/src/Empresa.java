package src;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Empresa{

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int empresaID;
    private String NIF;
    private String nome;
    private String morada;
    private String email;
    private int contacto;

    @OneToMany(mappedBy = "empresa")
    private List<Colaborador> colaboradores = new ArrayList<>();

    public Empresa(){

    }

    public Empresa( String NIF, String nome, String morada, String email, int contacto) {
        this.NIF = NIF;
        this.nome = nome;
        this.morada = morada;
        this.email = email;
        this.contacto = contacto;
    }

    public int getEmpresaID() {
        return empresaID;
    }

    public void setEmpresaID(int empresaID) {
        this.empresaID = empresaID;
    }

    public String getNIF() {
        return NIF;
    }

    public void setNIF(String NIF) {
        this.NIF = NIF;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getContacto() {
        return contacto;
    }

    public void setContacto(int contacto) {
        this.contacto = contacto;
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }

    public void addColaborador(Colaborador colaborador){
        colaboradores.add(colaborador);
    }

    public void setColaboradores(List<Colaborador> colaboradores) {
        this.colaboradores = colaboradores;
    }
}