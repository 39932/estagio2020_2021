package src.security.enums;

public enum Roles {
    ROLE_STUDENT, ROLE_TUTOR, ROLE_COLLABORATOR, ROLE_COMPANY, ROLE_ADMIN;

    @Override
    public String toString() {
        switch(this){
            case ROLE_STUDENT:
                return "ROLE_STUDENT";
            case ROLE_ADMIN:
                return "ROLE_ADMIN";
            case ROLE_TUTOR:
                return "ROLE_TUTOR";
            case ROLE_COLLABORATOR:
                return "ROLE_COLLABORATOR";
            case ROLE_COMPANY:
                return "ROLE_COMPANY";
        }

        return super.toString();
    }
}
