package src.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Qualifier("userDetailsServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Bean
    public BCryptPasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(encoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/candidaturas","/registo-aluno", "/registoAluno","/registo-empresa","/registoEmpresa","/registo-tutor","/registoTutor").permitAll()
                .antMatchers("/list-ordered-propostas","/list-update-Tutores","/list-update-Empresas","/list-update-Propostas","/list-update-Colaboradores","/list-update-Alunos","/criar-fase","/list-alunos","/list-candidaturas","/criar-proposta","/list-alunos-aval","/criar-colaborador","/criar-aluno","/criar-empresa","/criar-tutor").hasRole("ADMIN")
                .antMatchers("/list-ordered-propostas","/criar-colaborador").hasRole("COMPANY")
                .antMatchers("/list-ordered-propostas","/list-alunos","/list-candidaturas","/criar-proposta","/list-alunos-aval").hasRole("TUTOR")
                .antMatchers("/criar-proposta").hasRole("COLLABORATOR")
                .antMatchers("/candidaturas").hasRole("STUDENT")
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").usernameParameter("email").passwordParameter("password").permitAll();
    }
}
