package src.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import src.security.entities.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    AppUser findByEmail(String email);
}
