package src.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import src.security.entities.Role;
import src.security.enums.Roles;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByFunction(final Roles function);
}
