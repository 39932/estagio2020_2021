package src.security.entities;

import src.security.enums.Roles;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Roles function;

    @ManyToMany(mappedBy = "roles")
    private Collection<AppUser> appUsers;

    public Role() {

    }

    public Role(Roles function) {
        this.function = function;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Roles getFunction() {
        return function;
    }

    public void setFunction(Roles function) {
        this.function = function;
    }

    public Collection<AppUser> getAppUsers() {
        return appUsers;
    }

    public void setAppUsers(Collection<AppUser> appUsers) {
        this.appUsers = appUsers;
    }
}
