package src.security.service;

public interface SecurityService{
    boolean isAuthenticated();
    void handleLogin(final String username, final String password);
}
