package src.security.service;

import src.security.entities.AppUser;


public interface AppUserService {
    void saveStudent(AppUser appStudent);
    void saveTutor(AppUser appTutor);
    void saveCompany(AppUser appCompany);
    void saveCollaborator(AppUser appCollaborator);
    void saveAdmin(AppUser appAdmin);

    AppUser findByEmail(String email);


}
