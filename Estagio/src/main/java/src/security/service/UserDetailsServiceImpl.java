package src.security.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import src.security.entities.AppUser;
import src.security.repositories.AppUserRepository;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AppUserRepository appUserRepository;

    public UserDetailsServiceImpl(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    @Transactional(readOnly=true, rollbackFor = RuntimeException.class)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        final AppUser appUser = appUserRepository.findByEmail(username);

        if(appUser == null){
            throw new UsernameNotFoundException(username);
        }
        final Set<GrantedAuthority> grantedAuthoritySet = new HashSet<>();

        appUser.getRoles().forEach(r->grantedAuthoritySet.add(new SimpleGrantedAuthority(r.getFunction().toString())));

        return new User(appUser.getEmail(), appUser.getPassword(), grantedAuthoritySet);
    }
}
