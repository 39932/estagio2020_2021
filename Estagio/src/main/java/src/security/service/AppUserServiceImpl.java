package src.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import src.security.entities.AppUser;
import src.security.entities.Role;
import src.security.enums.Roles;
import src.security.repositories.AppUserRepository;
import src.security.repositories.RoleRepository;

import java.util.Optional;
import java.util.Set;

@Service
public class AppUserServiceImpl implements AppUserService {

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    AppUserRepository appUserRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public void saveStudent(AppUser appStudent) {
        appStudent.setPassword(encoder.encode(appStudent.getPassword()));
        appStudent.setRoles(Set.of(roleRepository.findByFunction(Roles.ROLE_STUDENT)));
        appUserRepository.save(appStudent);
    }

    @Override
    public void saveTutor(AppUser appTutor) {
        appTutor.setPassword(encoder.encode(appTutor.getPassword()));
        appTutor.setRoles(Set.of(roleRepository.findByFunction(Roles.ROLE_TUTOR)));
        appUserRepository.save(appTutor);
    }

    @Override
    public void saveCompany(AppUser appCompany) {
        appCompany.setPassword(encoder.encode(appCompany.getPassword()));
        appCompany.setRoles(Set.of(roleRepository.findByFunction(Roles.ROLE_COMPANY)));
        appUserRepository.save(appCompany);
    }

    @Override
    public void saveCollaborator(AppUser appCollaborator) {
        appCollaborator.setPassword(encoder.encode(appCollaborator.getPassword()));
        appCollaborator.setRoles(Set.of(roleRepository.findByFunction(Roles.ROLE_COLLABORATOR)));
        appUserRepository.save(appCollaborator);
    }

    @Override
    public void saveAdmin(AppUser appAdmin) {
        appAdmin.setPassword(encoder.encode(appAdmin.getPassword()));
        appAdmin.setRoles(Set.of(roleRepository.findByFunction(Roles.ROLE_ADMIN)));
        appUserRepository.save(appAdmin);
    }

    @Override
    public AppUser findByEmail(String email) {
        return Optional.ofNullable(appUserRepository.findByEmail(email)).orElse(new AppUser());
    }
}
