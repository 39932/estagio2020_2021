package src;

import javax.persistence.*;

@Entity
public class Vagas {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int vagasID;
    private int vagas;

    @ManyToOne
    private Fase fase;

    @ManyToOne
    private PropostasEstagio proposta;


    public Vagas(){

    }

    public Vagas(int vagasID, int vagas, Fase fase, PropostasEstagio proposta) {
        this.vagasID = vagasID;
        this.vagas = vagas;
        this.fase = fase;
        this.proposta = proposta;
    }

    public int getVagasID() {
        return vagasID;
    }

    public void setVagasID(int vagasID) {
        this.vagasID = vagasID;
    }

    public int getVagas() {
        return vagas;
    }

    public void setVagas(int vagas) {
        this.vagas = vagas;
    }

    public Fase getFase() {
        return fase;
    }

    public void setFase(Fase fase) {
        this.fase = fase;
    }

    public PropostasEstagio getProposta() {
        return proposta;
    }

    public void setProposta(PropostasEstagio proposta) {
        this.proposta = proposta;
    }
}