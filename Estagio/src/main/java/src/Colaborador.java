package src;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Colaborador{

    private String nome;
    @Id
    private String email;
    private String funcao;

    @OneToMany(mappedBy="colaborador")
    private List <Estagio> estagios = new ArrayList<>();

    @OneToMany(mappedBy="colaborador")
    private List <PropostasEstagio> propostasEstagio = new ArrayList<>();

    @ManyToOne
    private Empresa empresa;

    public Colaborador(){

    }

    public Colaborador(String nome, String email, String funcao) {
        this.nome = nome;
        this.email = email;
        this.funcao = funcao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public List<Estagio> getEstagios() {
        return estagios;
    }

    public void setEstagios(List<Estagio> estagios) {
        this.estagios = estagios;
    }

    public List<PropostasEstagio> getPropostasEstagio() {
        return propostasEstagio;
    }

    public void addPropostasEstagio(PropostasEstagio proposta){
        propostasEstagio.add(proposta);
    }

    public void setPropostasEstagio(List<PropostasEstagio> propostasEstagio) {
        this.propostasEstagio = propostasEstagio;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
}