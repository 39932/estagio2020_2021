package src.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import src.Tutor;

import java.util.Optional;

public interface TutorRepository extends JpaRepository<Tutor, String>{

    Optional<Tutor> findByEmail(String email);

}
