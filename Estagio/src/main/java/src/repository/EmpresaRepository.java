package src.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import src.Empresa;

import java.util.Optional;

public interface EmpresaRepository extends JpaRepository<Empresa, Integer>{
    Empresa findById(int id);
    Empresa findByNome(String nome);
    Empresa findByNIF(String NIF);

    Optional<Empresa> findByEmail(String email);

}
