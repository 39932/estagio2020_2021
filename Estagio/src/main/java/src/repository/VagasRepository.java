package src.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import src.Vagas;

public interface VagasRepository extends JpaRepository<Vagas, Integer>{
    Vagas findById(int id);
}
