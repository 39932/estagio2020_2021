package src.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import src.Fase;

public interface FaseRepository extends JpaRepository<Fase, Integer>{
    Fase findTopByOrderByNumeroFaseDesc();
}
