package src.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import src.Colaborador;

import java.util.Optional;

public interface ColaboradorRepository  extends JpaRepository<Colaborador, String>{
    Optional<Colaborador> findByEmail(String email);

}
