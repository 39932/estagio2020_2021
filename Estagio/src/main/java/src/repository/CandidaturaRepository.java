package src.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import src.Candidatura;

public interface CandidaturaRepository extends JpaRepository<Candidatura, Integer>{
    Candidatura findById(int id);
}
