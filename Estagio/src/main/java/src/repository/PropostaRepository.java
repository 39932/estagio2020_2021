package src.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import src.PropostasEstagio;

public interface PropostaRepository extends JpaRepository<PropostasEstagio, String>{
    PropostasEstagio findByNomeEstagio(String nomeEstagio);
}
