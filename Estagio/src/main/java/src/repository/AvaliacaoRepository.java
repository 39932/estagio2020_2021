package src.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import src.Avaliacao;

public interface AvaliacaoRepository extends JpaRepository<Avaliacao, Integer>{
    Avaliacao findById(int id);
}
