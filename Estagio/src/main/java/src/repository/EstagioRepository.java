package src.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import src.Estagio;

public interface EstagioRepository extends JpaRepository<Estagio, Integer>{
    Estagio findById(int id);
}
