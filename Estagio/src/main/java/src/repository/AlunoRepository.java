package src.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import src.Aluno;

import java.util.List;
import java.util.Optional;


public interface AlunoRepository extends JpaRepository<Aluno, Integer> {
	Aluno findById(int id);

    Optional<Aluno> findByEmail(String email);


    List<Aluno> findAll();
}
