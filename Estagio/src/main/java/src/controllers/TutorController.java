package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import src.*;
import src.repository.EstagioRepository;
import src.repository.TutorRepository;

import java.util.List;
import java.util.Set;

@Controller
public class TutorController {
    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);

    @Autowired
    TutorRepository tutorRepository;
    @Autowired
    EstagioRepository estagioRepository;

    @PostMapping(value = "/tutor")
    public String createTutor1(
            @RequestParam(name="email", required=true) String email, @RequestParam(name="nome",required = true) String nome,
            @RequestParam(name="escola", required = true) String escola, @RequestParam(name="departamento",required = true) String departamento,
            @RequestParam(name="contacto", required = true) Integer contacto,
            Model model){

        Tutor tutor1 = new Tutor(email,nome,escola,departamento,contacto);
        tutorRepository.save(tutor1);
        log.info("email  :" + email +" nome:"+ nome +" escola:" + escola + " departamento:" + departamento + " contacto:" + contacto);

        return "tutor";
    }

    @RequestMapping(value="/criar-tutor/{email}", method = RequestMethod.GET)
    public String updateTutor( @PathVariable("email") String email,Model model) {
        Tutor tutor = tutorRepository.findByEmail(email).orElse(null);

        model.addAttribute( "tutor",tutor);

        System.out.println(tutor.getNome());

        return "tutor";
    }

    @GetMapping("/list-update-Tutores")
    public String listTutores(Model model){
        List<Tutor> listTutores = tutorRepository.findAll();

        model.addAttribute("tutores",listTutores);

        return "list-tutores";
    }


    @GetMapping("/CreateTutor")
    public String createTutor(){
        log.info("Creating Tutor");

        Tutor tutor= new Tutor("ze@uevora.pt","ze","clav","informatica",912313312);
        tutorRepository.save(tutor);

        return "Adicionado tutor de nome " + tutor.getNome();
    }

    @GetMapping("AssociaTutorEstagio")
    public String associaTutorEstagio(Tutor tutor, List<Estagio> estagios){
        log.info("Associar Tutor e Estagios");

        for(Estagio estagio: estagios){
            estagio.setTutor(tutor);
            estagioRepository.save(estagio);

        }

        tutor.setEstagios(estagios);
        tutorRepository.save(tutor);

        return "Tutor e Estagios associados";

    }
}
