package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import src.*;

import src.repository.*;

import src.repository.AlunoRepository;
import src.repository.CandidaturaRepository;
import src.repository.FaseRepository;
import src.repository.PropostaRepository;

@RestController
public class TestController {

    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);

    @Autowired
    PropostaRepository propostaRepository;

    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    CandidaturaRepository candidaturaRepository;

    @Autowired

    ColaboradorRepository colaboradorRepository;

    @Autowired
    EmpresaRepository empresaRepository;

   /* @GetMapping("/testarProposta")

    FaseRepository faseRepository;

    @GetMapping("/testarProposta")

    public String testarProposta(){
        PropostasEstagio proposta= new PropostasEstagio("ola","fazercoisa","trabalhar e talz","joao","java",null);
        PropostasEstagio nova_proposta= new PropostasEstagio("adeus","nao trabalhar","dormir e talz","spring","java",null);
        propostaRepository.save(proposta);
        propostaRepository.save(nova_proposta);
        Aluno joao = new Aluno(29329,"joao miguel","29329@alunos.uevora.pt",966155680,12,"perfeito");
        alunoRepository.save(joao);
        Aluno miguel = new Aluno(329932,"miguel joao","39932@alunos.uevora.pt",966155680,17,"mais que perfeito");
        alunoRepository.save(miguel);
        Fase fase = faseRepository.findTopByOrderByNumeroFaseDesc();
        Candidatura candidato = new Candidatura(joao,proposta,fase);
        candidaturaRepository.save(candidato);
        miguel.addCandidatura(candidato);
        alunoRepository.save(miguel);

        log.info(candidato.getAluno().getNome());
        candidato.setAluno(miguel);
        log.info(candidato.getAluno().getNome());

        //proposta.setCandidatura(candidato);
        propostaRepository.save(proposta);

        log.info(propostaRepository.findByNomeEstagio("ola").getCandidatura().toString());


        return "Testes Feitos";

    }*/

    /*@GetMapping("/testarEmpresaProposta")
    public String testarEmpresaProposta(){
        PropostasEstagio proposta= new PropostasEstagio("ola","fazercoisa","trabalhar e talz","joao","java");
        PropostasEstagio nova_proposta= new PropostasEstagio("adeus","nao trabalhar","dormir e talz","spring","java");
        propostaRepository.save(proposta);
        propostaRepository.save(nova_proposta);
        Empresa empresa= new Empresa("1231313","asdasd","asdasdasd","email",19);
        empresaRepository.save(empresa);
        Colaborador colaborador = new Colaborador("francisco","l@l.pt","colaborador");
        colaboradorRepository.save(colaborador);

        proposta.setColaborador(colaborador);
        nova_proposta.setColaborador(colaborador);
        propostaRepository.save(proposta);
        propostaRepository.save(nova_proposta);


        colaborador.addPropostasEstagio(proposta);
        colaborador.addPropostasEstagio(nova_proposta);
        colaboradorRepository.save(colaborador);

        colaborador.setEmpresa(empresa);
        colaboradorRepository.save(colaborador);
        empresa.addColaborador(colaborador);
        empresaRepository.save(empresa);

        return "Exemplos hardcoded adicionados";
    }*/

    @GetMapping("/testarPropostasCandidaturas")
    public String testarPropostasCandidaturas(){
        PropostasEstagio proposta = new PropostasEstagio("Robot","fazer robots","fazendo robots","spring","boa nota pls");
        propostaRepository.save(proposta);
        Aluno Joao= new Aluno(39932,"joao","joao@sapo.pt",966155680,12,"Super bom");
        alunoRepository.save(Joao);
        Candidatura candidatura= new Candidatura(Joao,proposta);
        candidaturaRepository.save(candidatura);
        Aluno Miguel= new Aluno(40126,"Miguel","miguel@outlook.com",927802016,16,"umas fights");
        alunoRepository.save(Miguel);
        Candidatura candidatura1=new Candidatura(Miguel,proposta);
        candidaturaRepository.save(candidatura1);
        proposta.addCandidatura(candidatura);
        propostaRepository.save(proposta);
        proposta.addCandidatura(candidatura1);
        propostaRepository.save(proposta);



        return "Exemplos harcoded adicionados";
    }
}
