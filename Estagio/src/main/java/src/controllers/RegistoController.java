package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import src.Aluno;
import src.Empresa;
import src.EstagioApplication;
import src.Tutor;
import src.repository.AlunoRepository;
import src.repository.ColaboradorRepository;
import src.repository.EmpresaRepository;
import src.repository.TutorRepository;
import src.security.entities.AppUser;
import src.security.service.AppUserService;


@Controller
public class RegistoController {
    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);


    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    AppUserService appUserService;

    @Autowired
    EmpresaRepository empresaRepository;

    @Autowired
    ColaboradorRepository colaboradorRepository;

    @Autowired
    TutorRepository tutorRepository;

    @Autowired
    BCryptPasswordEncoder encoder;

    @PostMapping(value = "/registoAluno")
    public String registoAluno(
            @RequestParam(name="id", required=true) Integer id,
            @RequestParam(name="PrimeiroNome",required = true) String primeiroNome,
            @RequestParam(name="UltimoNome",required = true) String ultimoNome,
            @RequestParam(name="email", required = true) String email,
            @RequestParam(name="password", required = true) String password,
            @RequestParam(name="contacto",required = true) Integer contacto,
            @RequestParam(name="media", required = true) Integer media,
            @RequestParam(name="registo",required = true) String registo,
            Model model){

        System.out.println("CHEGUEI");
        AppUser user = new AppUser(primeiroNome, ultimoNome,email,password,true);
        appUserService.saveStudent(user);

        String nome = primeiroNome + " " + ultimoNome;

        Aluno aluno1 = new Aluno(id, nome, email, contacto, media, registo);
        alunoRepository.save(aluno1);
        log.info("nº de aluno:" + id +" nome:"+ nome +" email:" + email + " contacto:" + contacto + " media:" + media +
                " registo: " +registo);

        return "login";
    }

    @PostMapping(value = "/registoEmpresa")
    public String registoEmpresa(
            @RequestParam(name="nome",required = true) String nome,
            @RequestParam(name="email", required = true) String email,
            @RequestParam(name="password", required = true) String password,
            @RequestParam(name="nif",required = true) String nif,
            @RequestParam(name="contacto",required = true) Integer contacto,
            @RequestParam(name="morada",required = true) String morada,
            Model model){

        System.out.println("CHEGUEI");
        AppUser user = new AppUser(nome, "Company",email,password,true);
        appUserService.saveCompany(user);

        Empresa empresa1 = new Empresa(nif, nome, morada, email, contacto);
        empresaRepository.save(empresa1);
        log.info("nif:" + nif +" nome:"+ nome +" email:" + email + " contacto:" + contacto + "morada:"+morada);

        return "login";
    }

    @PostMapping(value = "/registoTutor")
    public String registoTutor(
            @RequestParam(name="email", required=true) String email,
            @RequestParam(name="password", required = true) String password,
            @RequestParam(name="nome",required = true) String nome,
            @RequestParam(name="escola", required = true) String escola,
            @RequestParam(name="departamento",required = true) String departamento,
            @RequestParam(name="contacto", required = true) Integer contacto,
            Model model){

        System.out.println("CHEGUEI");

        AppUser user = new AppUser(nome, "Tutor",email,password,true);
        appUserService.saveTutor(user);

        Tutor tutor1 = new Tutor(email,nome,escola,departamento,contacto);
        tutorRepository.save(tutor1);
        log.info("email  :" + email +" nome:"+ nome +" escola:" + escola + " departamento:" + departamento + " contacto:" + contacto);

        return "login";
    }
}
