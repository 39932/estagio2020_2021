package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import src.*;
import src.repository.AlunoRepository;
import src.repository.FaseRepository;
import src.repository.VagasRepository;
import src.security.repositories.AppUserRepository;

import java.util.List;

@RestController
public class VagasController {
    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);

    @Autowired
    VagasRepository vagasRepository;
    @Autowired
    FaseRepository faseRepository;

    @GetMapping("/CreateVagas")
    public String createVagas(){
        log.info("Creating Vagas");
        Vagas vagas= new Vagas(3,6,null,null);
        vagasRepository.save(vagas);

        return "Adicionado vagas  " + vagas.getVagasID();
    }

    @GetMapping("AssociaVagasFase")
    public String associaVagasFase(List<Vagas> vagas, Fase fase){
        log.info("Associar Vagas e Fase");

        fase.setVagasList(vagas);
        faseRepository.save(fase);

        for(Vagas vaga: vagas){
            vaga.setFase(fase);
            vagasRepository.save(vaga);

        }


        return "Vagas e Fase associados";

    }
}
