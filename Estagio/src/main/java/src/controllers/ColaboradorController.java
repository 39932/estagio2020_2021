package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import src.Aluno;
import src.Colaborador;
import src.Empresa;
import src.EstagioApplication;
import src.repository.ColaboradorRepository;
import src.repository.EmpresaRepository;
import src.security.entities.AppUser;
import src.security.service.AppUserService;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ColaboradorController {
    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);

    @Autowired
    EmpresaRepository empresaRepository;

    @Autowired
    ColaboradorRepository colaboradorRepository;

    @Autowired
    AppUserService appUserService;

    @PostMapping(value = "/colaborador")
    public String createColaborador1(
             @RequestParam(name="nome",required = true) String nome,
             @RequestParam(name="email", required = true) String email,
             @RequestParam(name="password", required = true) String password,
             @RequestParam(name="funcao",required = true) String funcao,
             Model model){

        System.out.println("CHEGUEI");
        AppUser user = new AppUser(nome, "Colaborador",email,password,true);
        appUserService.saveCollaborator(user);

        Colaborador colaborador1 = new Colaborador(nome, email,funcao);
        log.info("New colaborador name:" + nome +", email:" + email+", funcao:"+funcao);
        colaboradorRepository.save(colaborador1);


        return "colaborador";
    }

    @RequestMapping(value="/criar-colaborador/{email}", method = RequestMethod.GET)
    public String updateColaborador( @PathVariable("email") String email,Model model) {
        Colaborador colaborador2 = colaboradorRepository.findByEmail(email).orElse(null);

        model.addAttribute("colaborador",colaborador2);


        return "colaborador";
    }

    @GetMapping("/createColaborador")
    public String createColaborador(){
        Colaborador colaborador1= new Colaborador("Manuel","manuelEmail","apoia");
        Colaborador colaborador2= new Colaborador("Francisco","franciscoEmail","colabora");

        colaboradorRepository.save(colaborador2);
        colaboradorRepository.save(colaborador1);



        return "Colaborador Criado";

    }

    @GetMapping("/list-update-Colaboradores")
    public String listColabores(Model model){
        List<Colaborador> listcolaboradores = colaboradorRepository.findAll();

        model.addAttribute("colaboradores",listcolaboradores);

        return "list-colaboradores";
    }
}
