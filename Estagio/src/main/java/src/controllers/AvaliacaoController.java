package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import src.*;
import src.repository.AlunoRepository;
import src.repository.AvaliacaoRepository;
import src.repository.EstagioRepository;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AvaliacaoController {
    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);

    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    EstagioRepository estagioRepository;

    @Autowired
    AvaliacaoRepository avalRepository;

    @RequestMapping(value="/atribuirAval/{id}", method = RequestMethod.GET)
    public String updateAluno( @PathVariable("id") int id,Model model) {
        Aluno aluno2 = alunoRepository.findById(id);

        model.addAttribute( "aluno",aluno2);

        System.out.println(aluno2.getNome());

        return "aluno";
    }

    @GetMapping("/list-alunos-aval")
    public String mostraAluno(Model model){

        List<Aluno> listAlunos = alunoRepository.findAll();
        log.info(listAlunos.toString());

        model.addAttribute("alunos", listAlunos);
        return "alunos-view-aval";
    }

    @RequestMapping(value="/aluno-display-aval/{id}")
    public String product(Model model,@PathVariable(name = "id") int id) {
        Aluno aluno = alunoRepository.findById(id);
        List<Aluno> listAlunos = new ArrayList<>();

        listAlunos.add(aluno);
        log.info(listAlunos.toString());

        List<Candidatura> listCandidaturas =  aluno.getCandidaturas();
        model.addAttribute("alunos", listAlunos);

        model.addAttribute("propostas", listCandidaturas);

        return "aluno-display-aval";
    }

    @PostMapping(value = "/atribuir-aval")
    public String createEstagio(
            @RequestParam(name="NotaA", required=true) double NotaA,
            @RequestParam(name="NotaB",required = true) double NotaB,
            @RequestParam(name="NotaFinal", required = true) double NotaFinal,
            @RequestParam(name="numeroAluno",required = true) int numeroAluno,
            @RequestParam(name="coments",required = true) String coments,
            Model model){

        List<Avaliacao> avaliacaoList = new ArrayList<>();

        Aluno aluno = alunoRepository.findById(numeroAluno);

        Estagio estagio1 = aluno.getEstagio();

        Avaliacao avaliacao = new Avaliacao(coments,NotaA,NotaB,NotaFinal,estagio1);
        avaliacaoList.add(avaliacao);

        estagio1.setAvaliacoes(avaliacaoList);
         estagioRepository.save(estagio1);

         avaliacao.setEstagio(estagio1);
         avalRepository.save(avaliacao);


        return "index";
    }
}
