package src.controllers;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import src.*;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import src.Aluno;
import src.Estagio;
import src.EstagioApplication;
import src.repository.AlunoRepository;
import src.repository.CandidaturaRepository;
import src.repository.EstagioRepository;
import src.repository.PropostaRepository;
import src.security.entities.AppUser;
import src.security.service.AppUserService;

@Controller
public class AlunoController {
	
	private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);


	@Autowired
	AlunoRepository alunoRepository;

	@Autowired
	AppUserService appUserService;

	@PostMapping(value = "/aluno")
	public String createAluno1(
			@RequestParam(name="id", required=true) Integer id,
			@RequestParam(name="nome",required = true) String nome,
			@RequestParam(name="email", required = true) String email,
			@RequestParam(name="contacto",required = true) Integer contacto,
			@RequestParam(name="media", required = true) Integer media,
			@RequestParam(name="registo",required = true) String registo,
			Model model){

		Aluno aluno1 = new Aluno(id, nome, email, contacto, media, registo);
		alunoRepository.save(aluno1);
		log.info("nº de aluno:" + id +" nome:"+ nome +" email:" + email + " contacto:" + contacto + " media:" + media +
		" registo: " +registo);

		return "aluno";
	}




	@RequestMapping(value="/criar-aluno/{id}", method = RequestMethod.GET)
	public String updateAluno( @PathVariable("id") int id,Model model) {
		Aluno aluno2 = alunoRepository.findById(id);

		model.addAttribute( "aluno",aluno2);

		System.out.println(aluno2.getNome());
		
		return "aluno";
	}


	@GetMapping("/list-alunos")
	public String mostraAluno(Model model){

		List<Aluno> listAlunos = alunoRepository.findAll();
		log.info(listAlunos.toString());

		model.addAttribute("alunos", listAlunos);
		return "alunos-view";
	}

	@GetMapping("/list-update-Alunos")
	public String listAlunos(Model model){
		List<Aluno> listAlunos = alunoRepository.findAll();

		model.addAttribute("alunos",listAlunos);

		return "list-alunos";
	}


	@RequestMapping(value="/aluno-display/{id}")
	public String product(Model model,@PathVariable(name = "id") int id) {
		Aluno aluno = alunoRepository.findById(id);
		List<Aluno> listAlunos = new ArrayList<>();

		listAlunos.add(aluno);
		log.info(listAlunos.toString());

		List<Candidatura> listCandidaturas =  aluno.getCandidaturas();
		model.addAttribute("alunos", listAlunos);

		model.addAttribute("propostas", listCandidaturas);

		return "aluno-display";
	}

	@RequestMapping(value="/list-aluno-candidatura/{id}")
	public String listAlunoCandidatura(Model model , @PathVariable(name ="id") int id){
		Aluno aluno = alunoRepository.findById(id);

		List<Candidatura> listCandidaturas = new ArrayList<>();

		for(Candidatura candidatura:aluno.getCandidaturas()){
			listCandidaturas.add(candidatura);
		}

		model.addAttribute("candidaturas",listCandidaturas);

		return "list-candidaturas-aluno";
	}
}
