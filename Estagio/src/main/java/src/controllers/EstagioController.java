package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import src.*;
import src.repository.AlunoRepository;
import src.repository.EstagioRepository;
import src.repository.PropostaRepository;

import java.util.List;

@RestController
public class EstagioController {
    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);

    @Autowired
    EstagioRepository estagioRepository;

    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    PropostaRepository propostaRepository;

    @GetMapping("/atribuir-estagio")
    public String showProposta1(Model model){

        Estagio estagio = new Estagio();
        model.addAttribute("estagio", estagio);

        List<Aluno> listAlunos = alunoRepository.findAll();
        log.info(listAlunos.toString());

        model.addAttribute("alunos", listAlunos);
        return "estagio";
    }

    @PostMapping(value = "/atribuir-estagio")
    public String createEstagio(
            @RequestParam(name="dataInicio", required=true) String dataInicio,
            @RequestParam(name="dataFim",required = true) String dataFim,
            @RequestParam(name="duracao", required = true) int duracao,
            @RequestParam(name="numeroAluno",required = true) int numeroAluno,
            @RequestParam(name="nomeProposta", required = true) String nomeProposta,
            Model model){

        Aluno aluno = alunoRepository.findById(numeroAluno);
        PropostasEstagio proposta = propostaRepository.findByNomeEstagio(nomeProposta);

        Estagio estagio1 = new Estagio(dataInicio,dataFim,duracao,null,null,null,null,aluno);

        estagioRepository.save(estagio1);
        aluno.setEstagio(estagio1);
        alunoRepository.save(aluno);



        return "estagio";
    }


}
