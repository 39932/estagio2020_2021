package src.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import src.security.service.AppUserService;
import src.security.service.SecurityService;

@Controller
public class LoginController {

    @Autowired
    AppUserService appUserService;

    @Autowired
    SecurityService securityService;

    @GetMapping("/login")
    public String login(){
        if(securityService.isAuthenticated()){
            System.out.println("Auth");
            return "index";
        } else
            return "login";
    }
}
