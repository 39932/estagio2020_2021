package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import src.*;
import src.repository.AlunoRepository;
import src.repository.CandidaturaRepository;
import src.repository.FaseRepository;
import src.repository.PropostaRepository;

import java.util.ArrayList;

import java.util.HashSet;

import java.util.List;
import java.util.Set;

@Controller
public class PropostaController {

    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);

    @Autowired
    PropostaRepository propostaRepository;

    @Autowired
    FaseRepository faseRepository;

    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    CandidaturaRepository candidaturaRepository;

    @PostMapping(value = "/proposta")
    public String createProposta1(
            @RequestParam(name = "nome", required = true) String nome,
            @RequestParam(name = "descricao", required = true) String descricao,
            @RequestParam(name = "tarefas", required = true) String tarefas,
            @RequestParam(name = "ferramentas", required = true) String ferramentas,
            @RequestParam(name = "observacoes", required = true) String observacoes,
            Model model) {

        Fase fase = faseRepository.findTopByOrderByNumeroFaseDesc();
        Set<Fase> fases = new HashSet<>();
        fases.add(fase);
        PropostasEstagio proposta1 = new PropostasEstagio(nome, descricao, tarefas, ferramentas, observacoes, fases);
        propostaRepository.save(proposta1);
        log.info("nome:" + nome + " descricao:" + descricao + " tarefas:" + tarefas + " ferramentas:" + ferramentas + " observacoes:" + observacoes);

        return "proposta";
    }

    @PostMapping(value = "/list-to-order")
    public String updateList(@RequestParam(name = "ordem", required = true) String ordem, @RequestParam(name = "proposta", required = true) String proposta) {
        String[] split = ordem.split("[\\s]");

        PropostasEstagio proposta1 = propostaRepository.findByNomeEstagio(proposta);


        List<Candidatura> lista1 = new ArrayList<>();

        for (int i = 0; i < split.length; i++) {
            Candidatura candidatura1 = candidaturaRepository.findById(Integer.parseInt(split[i]));
            candidatura1.setPreferenciaEmpresa(i + 1);
            candidaturaRepository.save(candidatura1);

            lista1.add(candidatura1);
        }


        proposta1.setCandidatura(lista1);
        propostaRepository.save(proposta1);

        return "feedback";

    }



    @RequestMapping(value="/criar-proposta/{nomeEstagio}", method = RequestMethod.GET)
    public String updateProposta( @PathVariable("nomeEstagio") String nome,Model model) {
        PropostasEstagio proposta2 = propostaRepository.findByNomeEstagio(nome);

        model.addAttribute( "proposta",proposta2);


        return "proposta";
    }

    @GetMapping("/list-update-Propostas")
    public String listPropostas(Model model){
        List<PropostasEstagio> listPropostas = propostaRepository.findAll();

        model.addAttribute("propostas",listPropostas);

        return "list-propostas";
    }

    @GetMapping("/list-propostas")
    public String showProposta1(Model model){

        List<PropostasEstagio> listPropostas = propostaRepository.findAll();
        log.info(listPropostas.toString());
        
        model.addAttribute("propostas", listPropostas);
        return "propostas-view";
    }

    @GetMapping("/list-propostas-candidatura/{nomeEstagio}")
    public String listPropostasCandidatura(@PathVariable("nomeEstagio") String nome, Model model){
        PropostasEstagio proposta = propostaRepository.findByNomeEstagio(nome);

        List<Candidatura> lista1= new ArrayList<>();

        if(proposta.getCandidatura().get(0).getPreferenciaEmpresa()==0){
            model.addAttribute("candidaturas",proposta.getCandidatura());
        }

        else
        {
            for(int i=0;i<proposta.getCandidatura().size();i++){
                for(int j=0;j<proposta.getCandidatura().size();j++){
                    if(proposta.getCandidatura().get(j).getPreferenciaEmpresa()==i+1){
                        lista1.add(proposta.getCandidatura().get(j));
                    }
                }
            }

            model.addAttribute("candidaturas",lista1);
        }



        return "list-propostas-candidatura";
    }

    @GetMapping("/list-propostas-candidatura/{nomeEstagio}/{preferencia}")
    public String listPropostasCandidaturaFiltrar(@PathVariable("nomeEstagio")String nome,@PathVariable("preferencia") int preferencia,Model model){
        PropostasEstagio proposta=propostaRepository.findByNomeEstagio(nome);

        List<Candidatura> listaAux = new ArrayList<>();

        for (Candidatura candidatura :proposta.getCandidatura()){
            if(candidatura.getPreferenciasEstagios()==preferencia)
            {
                listaAux.add(candidatura);
            }
        }

        model.addAttribute("candidaturas",listaAux);

        return "list-propostas-candidatura";
    }

    @GetMapping("/list-ordered-propostas")
    public String listOrderedPropostas(Model model){


        List<PropostasEstagio> propostas= new ArrayList<>();

        for(PropostasEstagio proposta : propostaRepository.findAll()){
           if(proposta.isOrdered()){
               propostas.add(proposta);
           }
        }
        model.addAttribute("propostas",propostas);

        return"list-ordered-propostas";
    }

    @GetMapping("/list-to-order/{nomeEstagio}")
    public String listToOrder(@PathVariable("nomeEstagio")String nome,Model model){
        PropostasEstagio proposta = propostaRepository.findByNomeEstagio(nome);
        proposta.setOrdered(true);
        propostaRepository.save(proposta);

        List<Candidatura> lista1= new ArrayList<>();

        if(proposta.getCandidatura().get(0).getPreferenciaEmpresa()==0){
            model.addAttribute("candidaturas",proposta.getCandidatura());
        }

        else
        {
            for(int i=0;i<proposta.getCandidatura().size();i++){
                for(int j=0;j<proposta.getCandidatura().size();j++){
                    if(proposta.getCandidatura().get(j).getPreferenciaEmpresa()==i+1){
                        lista1.add(proposta.getCandidatura().get(j));
                    }
                }
            }

            model.addAttribute("candidaturas",lista1);
        }

        return "list-to-order";
    }








}
