package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import src.*;
import src.repository.ColaboradorRepository;
import src.repository.EmpresaRepository;

import java.util.ArrayList;
import java.util.List;

@Controller
public class EmpresaController {

    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);

    @Autowired
    EmpresaRepository empresaRepository;

    @Autowired
    ColaboradorRepository colaboradorRepository;

    @PostMapping(value = "/empresa")
    public String createEmpresa1(
            @RequestParam(name="nif", required=true) String nif, @RequestParam(name="nome",required = true) String nome,
            @RequestParam(name="email", required = true) String email, @RequestParam(name="contacto",required = true) Integer contacto,
            @RequestParam(name="morada", required = true) String morada){

        Empresa empresa1 = new Empresa(nif, nome, morada, email, contacto);


        empresaRepository.save(empresa1);


        return "empresa";
    }



    @RequestMapping(value="/criar-empresa/{id}", method= RequestMethod.GET)
    public String updateEmpresa(@PathVariable("id") int id, Model model){
        Empresa empresa = empresaRepository.findById(id);
        model.addAttribute("empresa",empresa);



        return "empresa";
    }


    @RequestMapping(value="/list-propostas-empresa/{id}", method= RequestMethod.GET)
    public String listPropostasEmpresa(@PathVariable("id")  int id, Model model) {
        Empresa empresa= empresaRepository.findById(id);
        List<Colaborador> ColaboradorList = empresa.getColaboradores();
        List<PropostasEstagio> propostasEmpresa = new ArrayList<>();

        for(Colaborador colaborador: ColaboradorList){
            for(PropostasEstagio propostasEstagio:colaborador.getPropostasEstagio()){
                propostasEmpresa.add(propostasEstagio);
            }

        }

        model.addAttribute("propostas",propostasEmpresa);
        return "list-propostas-empresa";
    }

    @GetMapping("/list-update-Empresas")
    public String listColabores(Model model){
        List<Empresa> listEmpresas = empresaRepository.findAll();

        model.addAttribute("empresas",listEmpresas);

        return "list-empresas";
    }

    @GetMapping("/createEmpresa")
    public String createEmpresa(){
        log.info("Creating Empresa");
        Empresa empresa = new Empresa( "505256155","MyCompany", "Evora", "mycopmany@mail.com", 965862325);
        empresaRepository.save(empresa);

        return "Empresa com o nome" + empresa.getNome() + "criada";

    }

    @GetMapping("/associaColaboradores")
    public String associaColaboradores(Empresa empresa, List<Colaborador> colaboradores){

        log.info("Associando colaboradores e empresas");

        for (Colaborador colab: colaboradores) {
            colab.setEmpresa(empresa);
            colaboradorRepository.save(colab);
        }
        empresa.setColaboradores(colaboradores);

        empresaRepository.save(empresa);

        return "Empresa com o nome" + empresa.getNome() + "criada e respetivos colaboradores";

    }


}
