package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import src.*;
import src.repository.*;

import java.util.*;

@Controller
public class CandidaturaController {

    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);

    @Autowired
    CandidaturaRepository candRepository;

    @Autowired
    EstagioRepository estagioRepository;

    @Autowired
    PropostaRepository propostaRepository;

    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    FaseRepository faseRepository;

    @PostMapping(value = "/create-candidatura")
    public String createCandidatura1(
            @RequestParam(name="nome1", required=true) String nome,
            @RequestParam(name="nome2",required = true) String nome2,
            @RequestParam(name="nome3", required = true) String nome3,
            @RequestParam(name="numero",required = true) int numero,
            Model model){

        Aluno aluno = alunoRepository.findById(numero);
        PropostasEstagio p1 = propostaRepository.findByNomeEstagio(nome);
        PropostasEstagio p2 = propostaRepository.findByNomeEstagio(nome2);
        PropostasEstagio p3 = propostaRepository.findByNomeEstagio(nome3);


        List<Candidatura> candidaturaList = new ArrayList<Candidatura>();

        Fase fase = faseRepository.findTopByOrderByNumeroFaseDesc();
        log.info("ID:" + fase.getNumeroFase() +" DataInicio:"+ fase.getDataInicio() +" DatFim:" + fase.getDataFim());

        Candidatura candidatura = new Candidatura(aluno,p1,fase);
        candRepository.save(candidatura);
        Candidatura candidatura2 = new Candidatura(aluno,p2,fase);
        candRepository.save(candidatura2);
        Candidatura candidatura3 = new Candidatura(aluno,p3,fase);
        candRepository.save(candidatura3);

        aluno.setCandidaturas(candidaturaList);

        alunoRepository.save(aluno);

        log.info("nome:" + p1.getNomeEstagio() +" nome2:"+ p2.getNomeEstagio() +" nome3:" + p3.getNomeEstagio() + " numero:" + aluno.getNumero());

        return "candidaturas";
    }

    @GetMapping(value = "/create-candidatura")
    public String createCandidatura(Model model){

        Candidatura candidatura = new Candidatura();
        model.addAttribute("candidatura", candidatura);

        List<PropostasEstagio> listPropostas = propostaRepository.findAll();
        model.addAttribute("propostas", listPropostas);

        return "candidaturas";
    }

    @GetMapping("/list-candidaturas")
    public String showProposta1(Model model){

        List<Candidatura> candidaturas = candRepository.findAll();
        log.info(candidaturas.toString());

        model.addAttribute("candidaturas", candidaturas);
        return "candidaturas-view";
    }
}
