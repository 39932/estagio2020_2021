package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import src.Aluno;
import src.EstagioApplication;
import src.Fase;
import src.repository.AlunoRepository;
import src.repository.FaseRepository;

@Controller
public class FaseController {
    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);


    @Autowired
    FaseRepository faseRepository;

    @PostMapping(value = "/fase")
    public String createAluno1(
            @RequestParam(name="numeroFase", required=true) Integer numeroFase,
            @RequestParam(name="dataInicio",required = true) String dataInicio,
            @RequestParam(name="dataFim", required = true) String dataFim,
            Model model){

            Fase fase = new Fase(numeroFase,dataInicio,dataFim);
            faseRepository.save(fase);
        return "fase";
    }
}
