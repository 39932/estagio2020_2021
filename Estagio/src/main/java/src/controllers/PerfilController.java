package src.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import src.*;
import src.repository.AlunoRepository;
import src.repository.ColaboradorRepository;
import src.repository.EmpresaRepository;
import src.repository.TutorRepository;
import src.security.entities.AppUser;
import src.security.entities.Role;
import src.security.enums.Roles;
import src.security.repositories.RoleRepository;
import src.security.service.AppUserService;
import src.security.service.SecurityService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class PerfilController {
    private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);
    @Autowired
    AlunoRepository alunoRepository;

    @Autowired
    SecurityService securityService;
    @Autowired
    AppUserService appUserService;

    @Autowired
    EmpresaRepository empresaRepository;

    @Autowired
    ColaboradorRepository colaboradorRepository;

    @Autowired
    TutorRepository tutorRepository;

    @Autowired
    RoleRepository roleRepository;

    @GetMapping(value="/perfil/{email}")
    public String product(Model model, @PathVariable(name = "email") String email) {

        System.out.println("CHEGOU AO CONTROLADOR");
        AppUser user = appUserService.findByEmail(email);
        Aluno aluno = alunoRepository.findByEmail(email).orElse(null);
        Tutor tutor = tutorRepository.findByEmail(email).orElse(null);
        Empresa empresa = empresaRepository.findByEmail(email).orElse(null);
        Colaborador colaborador = colaboradorRepository.findByEmail(email).orElse(null);

            if(aluno!=null) {
                model.addAttribute("aluno",aluno);
                return "aluno-perfil";
            }else if(tutor!=null) {
                model.addAttribute("tutor",tutor);
                return "tutor-perfil";
            }else if(empresa!=null) {
                model.addAttribute("empresa",empresa);
                return "empresa-perfil";
            }else if(colaborador!=null) {
                model.addAttribute("colaborador",colaborador);
                return "colaborador-perfil";
            }


        return "index";
    }
}
