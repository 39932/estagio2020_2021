package src;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Aluno{
    @Id
    private Integer id;
    private String nome;
    private String email;
    private Integer contacto;
    private Integer media;
    private String registoAcademico;

    @OneToOne
    private Estagio estagio;

    @OneToMany(mappedBy = "aluno")
    private List<Candidatura> candidaturas = new ArrayList<>();

    public Aluno(Integer numero, String nome, String email, Integer contacto, Integer media, String registoAcademico) {
        this.id = numero;
        this.nome = nome;
        this.email = email;
        this.contacto = contacto;
        this.media = media;
        this.registoAcademico = registoAcademico;
    }

    public Aluno(){

    }

    public Integer getNumero() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public Integer getContacto() {
        return contacto;
    }

    public Integer getMedia() {
        return media;
    }

    public String getRegistoAcademico() {
        return registoAcademico;
    }

    public Estagio getEstagio() {
        return estagio;
    }


    public List<Candidatura> getCandidaturas() {
        return candidaturas;
    }

    public void setNumero(Integer numero) {
        this.id = numero;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setContacto(Integer contacto) {
        this.contacto = contacto;
    }

    public void setMedia(Integer media) {
        this.media = media;
    }

    public void setRegistoAcademico(String registoAcademico) {
        this.registoAcademico = registoAcademico;
    }

    public void setEstagio(Estagio estagio) {
        this.estagio = estagio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void addCandidatura(Candidatura candidatura){
        candidaturas.add(candidatura);
    }


    public void setCandidaturas(List<Candidatura> candidaturas) {
        this.candidaturas = candidaturas;
    }
}