package src;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class PropostasEstagio{

    @Id
    private String nomeEstagio;
    private String descricao;
    private String tarefas;
    private String ferramentas;
    private String observacoes;
    private boolean ordered;

    @OneToMany(mappedBy="proposta")
    private List<Estagio> Estagios = new ArrayList<>();

    @OneToMany(mappedBy="proposta")
    private List<Vagas> vagasList = new ArrayList<>();

    @ManyToOne
    private Colaborador colaborador;

    @OneToMany(mappedBy = "proposta")
    private List<Candidatura> candidaturas= new ArrayList<>();

    @ManyToMany
    private Set<Fase> fase = new HashSet<>();

    public PropostasEstagio(){

    }



    public boolean isOrdered() {
        return ordered;
    }

    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }


    public PropostasEstagio(String nomeEstagio, String descricao, String tarefas, String ferramentas, String observacoes, Set<Fase> fase) {

        this.nomeEstagio = nomeEstagio;
        this.descricao = descricao;
        this.tarefas = tarefas;
        this.ferramentas = ferramentas;
        this.observacoes = observacoes;
        ordered=false;

        this.fase = fase;

    }

    public PropostasEstagio(String nomeEstagio, String descricao, String tarefas, String ferramentas, String observacoes) {

        this.nomeEstagio = nomeEstagio;
        this.descricao = descricao;
        this.tarefas = tarefas;
        this.ferramentas = ferramentas;
        this.observacoes = observacoes;
        ordered=false;

    }

    public String getNomeEstagio() {
        return nomeEstagio;
    }

    public void setNomeEstagio(String nomeEstagio) {
        this.nomeEstagio = nomeEstagio;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTarefas() {
        return tarefas;
    }

    public void setTarefas(String tarefas) {
        this.tarefas = tarefas;
    }

    public String getFerramentas() {
        return ferramentas;
    }

    public void setFerramentas(String ferramentas) {
        this.ferramentas = ferramentas;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public List<Estagio> getEstagios() {
        return Estagios;
    }

    public void setEstagios(List<Estagio> estagios) {
        Estagios = estagios;
    }

    public List<Vagas> getVagasList() {
        return vagasList;
    }

    public void setVagasList(List<Vagas> vagasList) {
        this.vagasList = vagasList;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public List<Candidatura> getCandidatura() {
        return candidaturas;
    }

    public void setCandidatura(List<Candidatura> candidaturas) {
        this.candidaturas = candidaturas;
    }

    public void addCandidatura( Candidatura candidatura){ candidaturas.add(candidatura);}

    public Set<Fase> getFase() {
        return fase;
    }

    public void setFase(Set<Fase> fase) {
        this.fase = fase;
    }
}