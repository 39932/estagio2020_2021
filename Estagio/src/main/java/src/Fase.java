package src;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Fase{
    @Id
    private int numeroFase;
    private String dataInicio;
    private String dataFim;

    @OneToMany(mappedBy = "fase")
    private List<Candidatura> candidatura = new ArrayList<>();

    @OneToMany(mappedBy = "fase")
    private List<Vagas> vagasList = new ArrayList<>();

    @ManyToMany(mappedBy="fase")
    private Set<PropostasEstagio> propostas = new HashSet<>();

    public Fase(){

    }

    public Fase(int numeroFase, String dataInicio, String dataFim) {
        this.numeroFase = numeroFase;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
    }

    public int getNumeroFase() {
        return numeroFase;
    }

    public void setNumeroFase(int numeroFase) {
        this.numeroFase = numeroFase;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public List<Candidatura> getCandidatura() {
        return candidatura;
    }

    public void setCandidatura(List<Candidatura> candidatura) {
        this.candidatura = candidatura;
    }

    public List<Vagas> getVagasList() {
        return vagasList;
    }

    public void setVagasList(List<Vagas> vagasList) {
        this.vagasList = vagasList;
    }

    public Set<PropostasEstagio> getPropostas() {
        return propostas;
    }

    public void setPropostas(Set<PropostasEstagio> propostas) {
        this.propostas = propostas;
    }
}