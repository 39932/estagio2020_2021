package src;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Tutor{
    @Id
    private String email;
    private String nome;
    private String escola;
    private String departamento;
    private int contacto;
    @OneToMany(mappedBy = "tutor")
    private List<Estagio> estagios = new ArrayList<>();



    public Tutor(){

    }

    public Tutor(String email, String nome, String escola, String departamento, int contacto) {
        this.email = email;
        this.nome = nome;
        this.escola = escola;
        this.departamento = departamento;
        this.contacto = contacto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEscola() {
        return escola;
    }

    public void setEscola(String escola) {
        this.escola = escola;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public int getContacto() {
        return contacto;
    }

    public void setContacto(int contacto) {
        this.contacto = contacto;
    }

    public List<Estagio> getEstagios() {
        return estagios;
    }

    public void setEstagios(List<Estagio> estagios) {
        this.estagios = estagios;
    }
}