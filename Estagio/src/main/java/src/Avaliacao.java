package src;

import javax.persistence.*;

@Entity
public class Avaliacao{

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int ID;
    private String comentarios;
    private double NotaA;
    private double NotaB;
    private double NotaFinal;
    @ManyToOne
    private Estagio Estagio;


    public Avaliacao(String comentarios, double notaA, double notaB, double notaFinal, Estagio estagio) {
        this.comentarios = comentarios;
        NotaA = notaA;
        NotaB = notaB;
        NotaFinal = notaFinal;
        Estagio = estagio;
    }

    public Avaliacao(){

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public double getNotaA() {
        return NotaA;
    }

    public void setNotaA(Float notaA) {
        NotaA = notaA;
    }

    public double getNotaB() {
        return NotaB;
    }

    public void setNotaB(Float notaB) {
        NotaB = notaB;
    }

    public double getNotaFinal() {
        return NotaFinal;
    }

    public void setNotaFinal(Float notaFinal) {
        NotaFinal = notaFinal;
    }

    public Estagio getEstagio() {
        return Estagio;
    }

    public void setEstagio(Estagio estagio) {
        this.Estagio = estagio;
    }
}
