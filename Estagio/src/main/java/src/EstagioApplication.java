package src;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import src.repository.*;
import src.security.entities.AppUser;
import src.security.entities.Role;
import src.security.enums.Roles;
import src.security.repositories.AppUserRepository;
import src.security.repositories.RoleRepository;

import java.util.*;

@SpringBootApplication
public class EstagioApplication {

	private static final Logger log = LoggerFactory.getLogger(EstagioApplication.class);


	public static void main(String[] args) {

		SpringApplication.run(EstagioApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(BCryptPasswordEncoder encoder, AppUserRepository appUserRepository, RoleRepository roleRepository, AlunoRepository alunoRepository, PropostaRepository propostaRepository, FaseRepository faseRepository) {
		return (args) -> {

			Fase fase1 = new Fase(1,"Janeiro","Fevereiro");
			faseRepository.save(fase1);

			Fase fase2 = new Fase(2,"Março","Abril");
			faseRepository.save(fase2);

			Set<Fase> fases = new HashSet<>();
			fases.add(fase1);
			fases.add(fase2);
		    log.info("Creating a student");
		    Aluno aluno1 = new Aluno(12345, "Dr Who", "email", 96123456, 12, "registo");
		    
		    alunoRepository.save(aluno1);

			for (Aluno c : alunoRepository.findAll()) {
		          System.out.println(c.getNome());
		    }

			PropostasEstagio proposta1 = new PropostasEstagio("Empresa 1","Estagio na empresa 1","Tarefas","Ferramentas","Observaçoes",fases);
			PropostasEstagio proposta2 = new PropostasEstagio("Empresa 2","Estagio na empresa 2","Tarefas","Ferramentas","Observaçoes",fases);
			PropostasEstagio proposta3 = new PropostasEstagio("Empresa 3","Estagio na empresa 3","Tarefas","Ferramentas","Observaçoes",fases);
			propostaRepository.save(proposta1);
			propostaRepository.save(proposta2);
			propostaRepository.save(proposta3);

			Role roleStudent = new Role(Roles.ROLE_STUDENT);
			roleRepository.save(roleStudent);
			Role roleAdmin = new Role(Roles.ROLE_ADMIN);
			roleRepository.save(roleAdmin);
			Role roleCollaborator = new Role(Roles.ROLE_COLLABORATOR);
			roleRepository.save(roleCollaborator);
			Role roleCompany = new Role(Roles.ROLE_COMPANY);
			roleRepository.save(roleCompany);
			Role roleTutor = new Role(Roles.ROLE_TUTOR);
			roleRepository.save(roleTutor);

			AppUser appUser = new AppUser("Joao","Galvao","joaoandre.galvao@gmail.com",encoder.encode("password"),true,Set.of(roleRepository.findByFunction(Roles.ROLE_STUDENT)));
			appUserRepository.save(appUser);

			AppUser appUser1 = new AppUser("José","Ferreira","jose@gmail.com",encoder.encode("password"),true,Set.of(roleRepository.findByFunction(Roles.ROLE_TUTOR)));
			appUserRepository.save(appUser1);

			AppUser appUser2 = new AppUser("Carlos","Rabeta","carlos@gmail.com",encoder.encode("password"),true,Set.of(roleRepository.findByFunction(Roles.ROLE_TUTOR),roleRepository.findByFunction(Roles.ROLE_STUDENT)));
			appUserRepository.save(appUser2);

		};
	}

	@Bean
	public CommandLineRunner testeAula(PropostaRepository propostaRepository,CandidaturaRepository candidaturaRepository,AlunoRepository alunoRepository,ColaboradorRepository colaboradorRepository,TutorRepository tutorRepository,EmpresaRepository empresaRepository){
		return (args)->{
			PropostasEstagio proposta1 = new PropostasEstagio("ola","Realizar um app","Tratar do login,tratar da implementacao","Spring","1 vaga");
			propostaRepository.save(proposta1);
			Aluno Joao= new Aluno(37814,"Joao","joaoandre.galvao@gmail.com",966155680,12,"Super bom");
			alunoRepository.save(Joao);
			Aluno Miguel= new Aluno(40126,"Miguel","miguel@outlook.com",927802016,16,"umas fights");
			alunoRepository.save(Miguel);
			Aluno Francisco = new Aluno(42575,"Francisco","franciscopedro_7@hotmail.com",963139129,14,"temper desmemedido");
			alunoRepository.save(Francisco);
			Colaborador Pedro = new Colaborador("Pedro","pedro@hotmail.com","engenheiro");
			colaboradorRepository.save(Pedro);
			Colaborador Marco= new Colaborador("Marco","Marco@gmail.com","CEO");
			colaboradorRepository.save(Marco);
			Empresa Cork= new Empresa("7897213213-2","Cork LDA","Rua do Carvalheiro","Cork@hotmail.com",913452312);
			empresaRepository.save(Cork);
			Empresa MicroInformatica= new Empresa("2345655343","Micro Corp","Rua Vitorino Nemésio","micro_corp@hotmail.com",912321123);
			empresaRepository.save(MicroInformatica);
			Tutor Salvador = new Tutor("Salvador","salvas@hotmail.com","clav","departamento_informatica",966156982);
			tutorRepository.save(Salvador);


			Candidatura candidatura1= new Candidatura(Joao,proposta1);
			candidaturaRepository.save(candidatura1);
			candidatura1.setPreferenciasEstagios(1);
			candidaturaRepository.save(candidatura1);

			Candidatura candidatura2= new Candidatura(Miguel,proposta1);
			candidaturaRepository.save(candidatura2);
			candidatura2.setPreferenciasEstagios(2);
			candidaturaRepository.save(candidatura2);

			Candidatura candidatura3= new Candidatura(Francisco,proposta1);
			candidaturaRepository.save(candidatura3);
			candidatura3.setPreferenciasEstagios(3);
			candidaturaRepository.save(candidatura3);

			Pedro.addPropostasEstagio(proposta1);
			colaboradorRepository.save(Pedro);
			Pedro.setEmpresa(Cork);
			colaboradorRepository.save(Pedro);
			Cork.addColaborador(Pedro);
			empresaRepository.save(Cork);
			proposta1.setColaborador(Pedro);
			propostaRepository.save(proposta1);
		};
	}

	@Bean
	public CommandLineRunner demo1(EstagioRepository estagioRepository, AlunoRepository alunoRepository) {
		return (args) -> {

			log.info("Creating Estagio");
			Estagio estagio1 = new Estagio("10/12/18", "20", 720, null, null, null, null, null);
			estagioRepository.save(estagio1);
			
			for (Aluno aluno : alunoRepository.findAll()) {
				System.out.println(aluno.getNome());
				aluno.setEstagio(estagio1);
				alunoRepository.save(aluno);

				estagio1.setAluno(aluno);
				estagioRepository.save(estagio1);
				
				Estagio umEstagio= aluno.getEstagio();
				
				System.out.println("Um estagio: " + umEstagio.getDataInicio());

				System.out.println(aluno.getEstagio().getDataInicio());
			}
			
			for (Estagio e : estagioRepository.findAll()) {
				System.out.println(e.getDuracao());
			}

		};
	}






}
